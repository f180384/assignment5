package com.example.weatherapp;

import androidx.annotation.RequiresPermission;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface WeatherDAO {

    @Insert
    void insertWeather(Weather data);

    @Update
    void updateWeather(Weather data);


    @Query("SELECT * FROM Weather")
    List<Weather> getalldata();
}