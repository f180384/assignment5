package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class RoomData extends AppCompatActivity {


    Button main;
    TextView data;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_data);

        main.findViewById(R.id.main);
        data.findViewById(R.id.dataholder);
        new bgthread().start();

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Main_Int = new Intent(RoomData.this, MainActivity.class);
                startActivity(Main_Int);
                finish();
            }
        });

    }
    class bgthread extends Thread
    {
        public void run()
        {
            super.run();
            WeatherDatabase db = Room.databaseBuilder(getApplicationContext(),WeatherDatabase.class,"room_db").build();
            WeatherDAO weatherDAO = db.weatherDAO();

            List <Weather> weathers = weatherDAO.getalldata();
            String str ="";


            for (Weather weather : weathers)
                str=str+"\t"+weather.getId()+" "+weather.getCountry()+" "+weather.getCity()+" "+weather.getTmeper()+" "+weather.getSpeed()+" "+weather.getWdir()+"\n";
            data.setText(str);
        }
    }


}