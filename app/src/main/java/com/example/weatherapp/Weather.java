package com.example.weatherapp;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Weather {

    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name="city")
    public String City;
    @ColumnInfo(name="country")
    public String Country;
    @ColumnInfo(name="temp")
    public double tmeper;
    @ColumnInfo(name="decs")
    public String decs;
    @ColumnInfo(name="speed")
    public String speed;
    @ColumnInfo(name="winddir")
    public String wdir;

    public Weather()
    {

    }
    public Weather(String city,String con, double tem, String des, String spe, String dir)
    {
        this.City=city;
        this.Country=con;
        this.decs=des;
        this.tmeper=tem;
        this.wdir=dir;
        this.speed=spe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public double getTmeper() {
        return tmeper;
    }

    public void setTmeper(double tmeper) {
        this.tmeper = tmeper;
    }

    public String getDecs() {
        return decs;
    }

    public void setDecs(String decs) {
        this.decs = decs;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getWdir() {
        return wdir;
    }

    public void setWdir(String wdir) {
        this.wdir = wdir;
    }
}
