package com.example.weatherapp;

import androidx.room.Database;
import androidx.room.RoomDatabase;
@Database(entities = Weather.class,version = 1)
public abstract class WeatherDatabase extends RoomDatabase{

    private static WeatherDatabase instance;
    public abstract WeatherDAO weatherDAO();
}
